//
// This program should make the car go forward until it sees something about 10CM away.
// Once it sees an object in its path, it should spin
// 45 degress to the right
// then start going forward again( provided that there is not something else in its path
//

#include <NewPing.h>
#include <Servo.h>

#define TRIGGER_PIN  12  
#define ECHO_PIN     11 
#define MAX_DISTANCE 200 // Maximum distance we want to ping for (in centimeters). Maximum sensor distance is rated at 400-500cm.
  
  Servo lServo;
  Servo rServo;
  const int redLed = 2;
  const int greenLed = 4;
  int spinBit = 0; //0 = right; 1 = left
  
  NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE); // NewPing setup of pins and maximum distance.

  void setup() {
    lServo.attach(9);
    rServo.attach(10);
    pinMode(redLed, OUTPUT);
    pinMode(greenLed, OUTPUT);
    Serial.begin(9600);
  }
  
  void loop() {
    delay(50);                      // Wait 50ms between pings (about 20 pings/sec). 29ms should be the shortest delay between pings.
    
    // There also also APIs 
    // - sonar.ping_cm()
    // - sonar.ping_in() 
    // but I get inconsistant readings from them so I will stick with calculating cm
    
    unsigned int uS = sonar.ping(); // Send ping, get ping time in microseconds (uS).
    int distance = uS / US_ROUNDTRIP_CM; // calculate CM. 
    
    Serial.println(distance);// Log it for debug purposes
    
    if(distance < 10){ // so in less than 10cm I am going to crash into something.. What to do?
      stp(10); // stop
      spinRight(90); //spin 45 deg (ok, not exactally 45, but my feeble delay system)
    } else {
      forward(100); // otherwise all is good. Not sure about to 100... Maybe change based on detection since delay is blocking
    }
  }
  
  void spin(){
    if (spinBit ==0){
      spinRight(90);
      spinBit=1;
    } else if(spinBit ==1){
      spinLeft(90);
      spinBit=0;
    }
  }
    
  
  void lightUp(String clr){
    // I was having such a hard time figuring out what was going on, so I installed some lights to see what's what
    // This is my easy light up system using a string to select. A string seemed more user-friendly than an int.
    if (clr == "red"){
      digitalWrite(redLed, HIGH);
      digitalWrite(greenLed, LOW);
    } else if (clr == "green"){
      digitalWrite(redLed, LOW);
      digitalWrite(greenLed, HIGH);
    }
  }
  
  void forward(int delAmt){
    // go forward
    // I am using 100 because it's a nice round number but 80 seems to be the same speed.
      lightUp("green");
      lServo.write(-100);
      rServo.write(-100);
  
      delay(delAmt);
  }
  
  void stp(int delAmt){
    // go backward.
    // no idea why sending "0" does not stop it. but "43" does? I need to learn more about these servos and such
      lightUp("red");
      lServo.write(43);
      rServo.write(43);
      delay(delAmt);
  }
  
  void spinRight(int spinNum){
    // Yeah, this is crazy. I take a degree and spin to that amount based on delays and such.
    // Totally based on my visual observations on my hadrwood floor at home. Not an exact measurement at all.
    // one number is positive, the other is negative. This makes the wheels move at the smae speed in oposite directions.
    
    lightUp("red");
    int delAmt = 0;
    lServo.write(100);
    rServo.write(-100);
    
    switch(spinNum){
      case 45 :
        delAmt = 385;
        break;
      case 90:
        delAmt = 845;
        break;
      case 180 :
        delAmt = 1540;  
        break;
    };
    delay(delAmt);
  }
  
  void spinLeft(int spinNum){
    // Yeah, this is crazy. I take a degree and spin to that amount based on delays and such.
    // Totally based on my visual observations on my hadrwood floor at home. Not an exact measurement at all.
    // one number is positive, the other is negative. This makes the wheels move at the smae speed in oposite directions.
    
    lightUp("red");
    int delAmt = 0;
    lServo.write(-100);
    rServo.write(100);
    
    switch(spinNum){
      case 45 :
        delAmt = 385;
        break;
      case 90:
        delAmt = 845;
        break;
      case 180 :
        delAmt = 1540;  
        break;
    };
    delay(delAmt);
  }

#include <Servo.h>
Servo lServo;
Servo rServo; // wont go backwards anymore
int pwmL,pwmR;
int increment = 100;
const int redLed = 2;
const int greenLed = 4;

void setup(){
  lServo.attach(9);
  rServo.attach(10);
  pinMode(redLed, OUTPUT);
  pinMode(greenLed, OUTPUT);
  Serial.begin(9600);
}

void spinRight(int spinNum){
     digitalWrite(redLed, LOW);
    digitalWrite(greenLed, HIGH);
    int delAmt = 0;
    lServo.write(pwmL + increment);
    rServo.write(pwmR - increment);
    switch(spinNum){
      case 45 :
        delAmt = 385;
        break;
      case 90:
        delAmt = 845;
        break;
      case 180 :
        delAmt = 1540;  
        break;
    };
    delay(delAmt);
}

void spinLeft(int spinNum){
    digitalWrite(redLed, LOW);
    digitalWrite(greenLed, HIGH);
    int delAmt = 0;
    lServo.write(pwmL - increment);
    rServo.write(pwmR + increment);
    switch(spinNum){
      case 45 :
        delAmt = 385;
        break;
      case 90:
        delAmt = 845;
        break;
      case 180 :
        delAmt = 1540;  
        break;
    };
    delay(delAmt);
}

void forward(int delAmt){
    digitalWrite(redLed, LOW);
    digitalWrite(greenLed, HIGH);
    lServo.write(-100);
    rServo.write(-100);

    delay(delAmt);
}

void stp(int delAmt){
    digitalWrite(redLed, HIGH);
    digitalWrite(greenLed, LOW);
    lServo.write(43);
    rServo.write(43);
    delay(delAmt);
}

void rightSquare(){
  forward(1000);
  spinRight(90);
  forward(1000);
  spinRight(90);
  forward(1000);
  spinRight(90);
  forward(1000);
  spinRight(90);
}

void leftSquare(){
  forward(1000);
  spinLeft(90);
  forward(1000);
  spinLeft(90); 
  forward(1000);
  spinLeft(90);
  forward(1000);
  spinLeft(90);
}
  

void loop(){
  leftSquare();
  stp(1000);
  rightSquare();
  stp(1000);
}


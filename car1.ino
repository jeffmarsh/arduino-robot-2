#include <Servo.h>
Servo lServo;
Servo rServo;
int const potPin =A0;
int x =0;
int potVal;
int angle;
int pwmL,pwmR;
int ctrl = 0;
void setup(){
  lServo.attach(9);
  rServo.attach(10);
  Serial.begin(9600);
}
void loop(){
  Serial.println(ctrl);
  if(ctrl < 1500){
    //go forward
    lServo.write(pwmL -110);
    rServo.write(pwmR + 110);
  } else if (ctrl > 1500 && ctrl < 2000){
    //spin
    lServo.write(pwmL + 110);
    rServo.write(pwmR + 110);
  } else if(ctrl >2001){
    ctrl = 0;
  }
  ctrl++;
}

